<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Register user
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'gender' => 'required',
            'birthdate' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        $user = $this->userRepository->create($validatedData);
        $token = $user->createToken('authToken')->token();

        return view('home', ['error' => null]);
    }

    /**
     * Login user
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($validatedData)) {
            return view('home', ['error' => 'Invalid Credentials']);
        }

        $tokenObject = auth()->user()->createToken('authToken');
        $accessToken = $tokenObject->accessToken;
        $expires_at = date('Y-m-d H:i:s', strtotime($tokenObject->token->expires_at));

        return redirect('/companies');
    }
}
