<?php

namespace App\Http\Controllers;

use App\Repositories\CompanyRepositoryInterface;
use App\Repositories\DepartmentRepositoryInterface;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    private $companyRepository;

    private $departmentRepository;

    public function __construct(CompanyRepositoryInterface $companyRepository, DepartmentRepositoryInterface $departmentRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * List all companies
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $companies = $this->companyRepository->list();
        return view('companies', ['companies' => $companies]);
    }

    /**
     * Create company
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'address' => 'required',
        ]);

        $this->companyRepository->create($validatedData);
        $companies = $this->companyRepository->list();
        return view('companies', ['companies' => $companies]);
    }

    /**
     * Create Screen
     *
     * @return \Illuminate\Http\Response
     */
    public function createScreen()
    {
        return view('create-company', ['type' => 'add', 'company' => []]);
    }

    /**
     * Edit company
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer',
            'name' => 'required|string',
            'address' => 'required|string',
        ]);

        $this->companyRepository->update($validatedData);
        $companies = $this->companyRepository->list();
        return view('companies', ['companies' => $companies]);
    }

    /**
     * Edit company details
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editScrewn(int $id)
    {
        $company = $this->companyRepository->getById($id);
        return view('create-company', ['type' => 'edit', 'company' => $company]);
    }

    /**
     * Get company
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get(int $id)
    {
        $company = $this->companyRepository->getById($id);
        $departments = $this->departmentRepository->getCompanyDepartments($id);
        return view('create-company', ['type' => 'get', 'company' => $company, 'departments' => $departments]);
    }

    /**
     * Delete Company
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer'
        ]);

        $this->companyRepository->delete($validatedData['id']);
        $companies = $this->companyRepository->list();
        return view('companies', ['companies' => $companies]);
    }
}
