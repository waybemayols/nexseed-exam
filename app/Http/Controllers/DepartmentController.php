<?php

namespace App\Http\Controllers;

use App\Repositories\CompanyRepositoryInterface;
use App\Repositories\DepartmentRepositoryInterface;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    private $departmentRepository;

    private $companyRepository;

    public function __construct(DepartmentRepositoryInterface $departmentRepository, CompanyRepositoryInterface $companyRepository)
    {
        $this->departmentRepository = $departmentRepository;
        $this->companyRepository = $companyRepository;
    }

    /**
     * List all departments
     *
     * @return \Illuminate\Http\Response
     */
    public function list(int $company_id = null)
    {
        $departments = [];
        $company = [];
        if (!empty($company_id)) {
            $departments = $this->departmentRepository->getCompanyDepartments($company_id);
            $company = $this->companyRepository->getById($company_id);
        } else {
            $departments = $this->departmentRepository->list();
        }
        return view('departments', ['departments' => $departments, 'company' => $company]);
    }

    /**
     * Create department
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'company_id' => 'required|integer',
        ]);

        if (empty($this->companyRepository->getById($validatedData['company_id']))) {
            return view('create-department', ['error' => 'Company does not exist']);
        }

        $this->departmentRepository->create($validatedData);
        $departments = $this->departmentRepository->list();
        return view('departments', ['departments' => $departments]);
    }

    /**
     * Create Screen
     *
     * @return \Illuminate\Http\Response
     */
    public function createScreen()
    {
        return view('create-department', ['type' => 'add', 'department' => []]);
    }

    /**
     * Edit department
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer',
            'name' => 'required',
            'company_id' => 'required|integer',
        ]);

        $this->departmentRepository->update($validatedData);
        $departments = $this->departmentRepository->list();
        return view('departments', ['departments' => $departments]);
    }

    /**
     * Edit department details
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editScreen(int $id)
    {
        $department = $this->departmentRepository->getById($id);
        return view('create-department', ['type' => 'edit', 'department' => $department]);
    }

    /**
     * Delete department
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->departmentRepository->delete($id);
        $departments = $this->departmentRepository->list();
        return view('departments', ['departments' => $departments]);
    }
}
