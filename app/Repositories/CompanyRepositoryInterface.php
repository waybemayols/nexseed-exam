<?php

namespace App\Repositories;

use App\Models\Company;

/**
* Interface CompanyRepositoryInterface
* @package App\Repositories
*/
interface CompanyRepositoryInterface
{
   /**
    * @param array $data
    * @return Company
    */
   public function create(array $data);

   /**
    * @param array $data
    * @return Company
    */
   public function update(array $data);

   /**
    * @param array $attributes
    * @return Company[]
    */
   public function list();

   /**
    * @param int $id
    * @return Company
    */
   public function getById(int $id);

   /**
    * @param int $id
    * @return bool
    */
   public function delete(int $id);
}
