<?php

namespace App\Repositories;

use App\Models\DepartmentEmployee;

/**
* Interface DepartmentEmployeeRepositoryInterface
* @package App\Repositories
*/
interface DepartmentEmployeeRepositoryInterface
{
   /**
    * @param array $data
    * @return DepartmentEmployee
    */
   public function create(array $data);

   /**
    * @param array $data
    * @return DepartmentEmployee
    */
   public function update(array $data);

   /**
    * @param int $employee_id
    * @return Department[]
    */
   public function getEmployeeDepartments(int $employee_id);

   /**
    * @param int $department_id
    * @return Employee[]
    */
   public function getDepartmentEmployees(int $department_id);
}
