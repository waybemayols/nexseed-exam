<?php

namespace App\Repositories;

use App\Models\Department;

/**
* Interface DepartmentRepositoryInterface
* @package App\Repositories
*/
interface DepartmentRepositoryInterface
{
   /**
    * @param array $data
    * @return Department
    */
   public function create(array $data);

   /**
    * @param array $data
    * @return Department
    */
   public function update(array $data);

   /**
    * @param array $attributes
    * @return Department[]
    */
   public function list();

   /**
    * @param int $id
    * @return Department
    */
   public function getById(int $id);

   /**
    * @param int $id
    * @return bool
    */
   public function delete(int $id);

   /**
    * @param int $company_id
    * @return Department[]
    */
   public function getCompanyDepartments(int $company_id);
}
