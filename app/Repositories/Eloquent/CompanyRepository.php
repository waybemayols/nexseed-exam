<?php

namespace App\Repositories\Eloquent;

use App\Models\Company;
use Illuminate\Support\Str;
use App\Repositories\CompanyRepositoryInterface;

class CompanyRepository implements CompanyRepositoryInterface
{
   /**
    * @param array $attributes
    * @return Company
    */
    public function create(array $data)
    {
        return Company::create([
            'name' => $data['name'],
            'address' => $data['address']
        ]);
    }

   /**
    * @param array $attributes
    * @return Company
    */
    public function update(array $data)
    {
        $company = Company::where('id', $data['id'])->first();
        $company->name = $data['name'];
        $company->address = $data['address'];
        $company = $company->save();
        return $company;
    }

    /**
     * @param array $attributes
     * @return Company[]
     */
    public function list(int $page = null)
    {
        return Company::get();
    }

    /**
     * @param array $attributes
     * @return Company
     */
    public function getById(int $id)
    {
        return Company::where('id', $id)->first();
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function delete(int $id)
    {
        return Company::destroy($id);
    }
}
