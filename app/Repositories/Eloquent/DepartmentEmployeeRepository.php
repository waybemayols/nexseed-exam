<?php

namespace App\Repositories\Eloquent;

use App\Models\Department;
use App\Models\DepartmentEmployee;
use App\Models\Employee;
use App\Repositories\DepartmentEmployeeRepositoryInterface;

class DepartmentEmployeeRepository implements DepartmentEmployeeRepositoryInterface
{
   /**
    * @param array $attributes
    * @return Department
    */
    public function create(array $data)
    {
        return DepartmentEmployee::create([
            'department_id' => $data['department_id'],
            'employee_id' => $data['employee_id']
        ]);
    }

   /**
    * @param array $attributes
    * @return Department
    */
    public function update(array $data)
    {
        $department = DepartmentEmployee::where('id', $data['id'])->first();
        $department->department_id = $data['department_id'];
        $department->employee_id = $data['employee_id'];
        $department = $department->save();
        return $department;
    }

    /**
     * @param array $attributes
     * @return Department[]
     */
    public function getEmployeeDepartments(int $employee_id)
    {
    return Department::join('departments', 'departments.id', '=', 'department_employees.department_id')
                    ->where('department_employees.employee_id', $employee_id)
                    ->get();
    }

    /**
     * @param int $department_id
    * @return Employee[]
    */
    public function getDepartmentEmployees(int $department_id)
    {
        return Employee::join('employees', 'employees.id', '=', 'department_employees.employee_id')
                        ->where('department_employees.department_id', $department_id)
                        ->get();
    }
}
