<?php

namespace App\Repositories\Eloquent;

use App\Models\Department;
use App\Repositories\DepartmentRepositoryInterface;

class DepartmentRepository implements DepartmentRepositoryInterface
{
   /**
    * @param array $attributes
    * @return Department
    */
    public function create(array $data)
    {
        return Department::create([
            'name' => $data['name'],
            'company_id' => $data['company_id']
        ]);
    }

   /**
    * @param array $attributes
    * @return Department
    */
    public function update(array $data)
    {
        $department = Department::where('id', $data['id'])->first();
        $department->name = $data['name'];
        $department->company_id = $data['company_id'];
        $department = $department->save();
        return $department;
    }

    /**
     * @param array $attributes
     * @return Department[]
     */
    public function list(int $page = null)
    {
        return Department::get();
    }

    /**
     * @param array $attributes
     * @return Department
     */
    public function getById(int $id)
    {
        return Department::where('id', $id)->first();
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function delete(int $id)
    {
        return Department::destroy($id);
    }

    /**
     * @param array $attributes
     * @return Department[]
     */
    public function getCompanyDepartments(int $company_id)
    {
        return Department::where('company_id', $company_id)->get();
    }
}
