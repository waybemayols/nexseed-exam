<?php

namespace App\Repositories\Eloquent;

use App\Models\Employee;
use App\Repositories\EmployeeRepositoryInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{
   /**
    * @param array $attributes
    * @return Employee
    */
    public function create(array $data)
    {
        return Employee::create([
            'name' => $data['name'],
            'gender' => $data['gender'],
            'birthdate' => $data['birthdate']
        ]);
    }

   /**
    * @param array $attributes
    * @return Employee
    */
    public function update(array $data)
    {
        $employee = Employee::where('id', $data['id'])->first();
        $employee->name = $data['name'];
        $employee->gender = $data['gender'];
        $employee->birthdate = date('Y-m-d', strtotime($data['birthdate']));
        $employee = $employee->save();
        return $employee;
    }

    /**
     * @param array $attributes
     * @return Employee[]
     */
    public function list(int $page = null)
    {
        return Employee::get();
    }

    /**
     * @param array $attributes
     * @return Employee
     */
    public function getById(int $id)
    {
        return Employee::where('id', $id)->first();
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function delete(int $id)
    {
        return Employee::destroy($id);
    }
}
