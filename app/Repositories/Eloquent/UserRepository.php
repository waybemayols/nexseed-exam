<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use Illuminate\Support\Str;
use App\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
   /**
    * @param array $attributes
    * @return User
    */
    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'gender' => $data['gender'],
            'password' => bcrypt($data['password']),
            'birthdate' => date('Y-m-d', strtotime($data['birthdate'])),
            'email' => $data['email'],
            'email_verified_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
