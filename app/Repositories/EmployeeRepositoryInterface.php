<?php

namespace App\Repositories;

use App\Models\Employee;

/**
* Interface EmployeeRepositoryInterface
* @package App\Repositories
*/
interface EmployeeRepositoryInterface
{
   /**
    * @param array $data
    * @return Employee
    */
   public function create(array $data);

   /**
    * @param array $data
    * @return Employee
    */
   public function update(array $data);

   /**
    * @param array $attributes
    * @return Employee[]
    */
   public function list();

   /**
    * @param int $id
    * @return Employee
    */
   public function getById(int $id);

   /**
    * @param int $id
    * @return bool
    */
   public function delete(int $id);
}
