<?php

namespace App\Repositories;

use App\Models\User;

/**
* Interface UserRepositoryInterface
* @package App\Repositories
*/
interface UserRepositoryInterface
{
   /**
    * @param array $attributes
    * @return User
    */
   public function create(array $data);
}
