<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
        <div style="margin: 15px 15px 15px 15px;">
            <form action="/create-company" method="get">
                <button type="submit" class="btn btn-primary">Create company</button>
            </form>
            <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Address</th>
                <th scope="col">Date Created</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @foreach($companies as $company)
                    <tr>
                        <th scope="row">{{ $company->id }}</th>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->address }}</td>
                        <td>{{ date('F d, Y', strtotime($company->created_at)) }}</td>
                        <td>

                            <form action="{{ '/view-company/'. $company->id }}" method="get">
                                <button type="submit" class="btn btn-primary">View</button>
                            </form>

                            <form action="{{ '/edit-company/'. $company->id }}" method="get">
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </form>

                            <form action="/delete-company" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ $company->id }}">
                                <button type="submit" class="btn btn-primary">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
    </body>
</html>
