<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    </head>
    <body>
        <div style="margin: 15px 15px 15px 15px;">
            @if($type === "add")
                <form action="/create-company" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Name</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="name">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Address</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="address">
                    </div>
                    <button type="submit" class="btn btn-primary">Create</button>
                </form>
            @elseif($type === "edit")
                <form action="/edit-company" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{ $company->id }}">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Name</label>
                        <input type="text" class="form-control" name="name" value={{ $company->name }} maxlength="50">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Address</label>
                        <input type="text" class="form-control" name="address" value={{ $company->address }} maxlength="50">
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </form>
            @else
                <div class="form-group">
                    <label for="exampleFormControlInput1">ID</label>
                    <input type="text" class="form-control" name="id" value={{ $company->id }} maxlength="50" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Name</label>
                    <input type="text" class="form-control" name="name" value={{ $company->name }} maxlength="50" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Address</label>
                    <input type="text" class="form-control" name="address" value={{ $company->address }} maxlength="50" disabled>
                </div>
                <a href="/companies">Back</a>
                <br><bR>


                <!--Accordion wrapper-->
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                    <!-- Accordion card -->
                    <div class="card">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="headingOne1">
                        <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                        aria-controls="collapseOne1">
                        <h5 class="mb-0">
                            View Departments <i class="fas fa-angle-down rotate-icon"></i>
                        </h5>
                        </a>
                        <button type="submit" class="btn btn-primary" style="float: right !important">Add department</button>
                    </div>

                    <!-- Card body -->
                    <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
                        data-parent="#accordionEx">
                        <div class="card-body">
                            <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">Department Number</th>
                                <th scope="col">Name</th>
                                <th scope="col">Date Created</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach($departments as $department)
                                    <tr>
                                        <th scope="row">{{ $department->id }}</th>
                                        <td>{{ $department->name }}</td>
                                        <td>{{ date('F d, Y', strtotime($department->created_at)) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </table>
                        </div>
                    </div>

                    </div>
                    <!-- Accordion card -->

                </div>
                <!-- Accordion wrapper -->
            @endif
          </form>
        </div>
    </body>
</html>
