<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', ['error' => null]);
});

Route::get('/register', function () {
    return view('registration');
});

Route::get('/companies', 'App\Http\Controllers\CompanyController@list');
Route::get('/create-company', 'App\Http\Controllers\CompanyController@createScreen');
Route::post('/create-company', 'App\Http\Controllers\CompanyController@create');
Route::post('/edit-company', 'App\Http\Controllers\CompanyController@edit');
Route::post('/company', 'App\Http\Controllers\CompanyController@create');
Route::post('/delete-company', 'App\Http\Controllers\CompanyController@destroy');
Route::get('/view-company/{id}', 'App\Http\Controllers\CompanyController@get');
Route::get('/edit-company/{id}', 'App\Http\Controllers\CompanyController@editScrewn');

Route::post('/registerUser', 'App\Http\Controllers\AuthController@register');
Route::post('/login', 'App\Http\Controllers\AuthController@login');
